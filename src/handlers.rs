use std::sync::Mutex;
mod ex1_simple_routes;
mod ex2_shared_state;
mod ex3_postgres;
mod ex5_user_authentication;

// HttpServer automatically starts a number of HTTP workers,
// by default this number is equal to the number of physical CPUs
// in the system.
// Once the workers are created, they each receive a separate
// application instance to handle requests. Application state
// is not shared between the threads, and handlers are free to
// manipulate their copy of the state with no concurrency concerns.
// To share state between worker threads, use an Arc/Data.
// Special care should be taken once sharing and synchronization
// are introduced. In many cases, performance costs are
// inadvertently introduced as a result of locking the shared
// state for modifications.
struct AppState {
    string: Mutex<String>,
}

// ex5 session middleware which extends the remaining time to live of a cookie-based session
/// Middleware which extends the lifetime of the cookie-based user session.\
/// Note: I do not know at this point in time if the session being renewed is client-specific or all-clients-wide,
/// but I suppose it is the former, as I can read \
///     "In Actix web, a session is identified by a unique session ID that is stored in a cookie on the user's browser.",\
/// and the renew method says\
///     "Renews the session key, assigning existing session state to new key.".\
/// See https://docs.rs/actix-web-lab/latest/actix_web_lab/middleware/fn.from_fn.html for more middleware information.
async fn renew_session_ttl(
    session: actix_session::Session,
    req: actix_web::dev::ServiceRequest,
    next: actix_web_lab::middleware::Next<impl actix_web::body::MessageBody>,
) -> Result<actix_web::dev::ServiceResponse<impl actix_web::body::MessageBody>, actix_web::Error> {
    // preprocessing
    session.renew();
    // call next service
    next.call(req).await
}

#[tracing::instrument(skip_all)]
pub(super) async fn run_application(
    env: crate::Env,
    services: crate::Services,
    repos: crate::Repositories,
) -> Result<(), crate::AppError> {
    // prepare a global application state, see ex2
    let init_app_state_value = AppState {
        string: Default::default(),
    };
    // prepare a global application state, see ex2
    let init_app_state = actix_web::web::Data::new(init_app_state_value);
    // prepare the https / ssl configuration
    let mut builder = openssl::ssl::SslAcceptor::mozilla_intermediate(openssl::ssl::SslMethod::tls()).unwrap();
    builder.set_private_key_file("key.pem", openssl::ssl::SslFiletype::PEM).unwrap();
    builder.set_certificate_chain_file("cert.pem").unwrap();
    // prepare for user authentication using session cookies
    let secret_key = actix_web::cookie::Key::generate();
    let cookie_duration = actix_web::cookie::time::Duration::seconds(10);

    let app_closure = move || {
        actix_web::App::new()
            // set the global application state, see ex2
            .app_data(init_app_state.clone())
            // user authentication using session cookies:
            // [from official doc] install the identity framework first
            .wrap(actix_identity::IdentityMiddleware::default())
            // [from official doc] the identity system is built on top of sessions
            // [from official doc] you must install the session middleware to leverage `actix-identity`
            .wrap(
                actix_session::SessionMiddleware
                    ::builder(actix_session::storage::CookieSessionStore::default(), secret_key.clone())
                    .cookie_name("actix-auth".into())
                    .cookie_secure(true)
                    .session_lifecycle(actix_session::config::PersistentSession::default().session_ttl(cookie_duration.clone()))
                    .build()
            )
            .wrap(actix_web_lab::middleware::from_fn(renew_session_ttl))
            .service(actix_files::Files::new("/actix/static", "./static"))
            .service(
                actix_web::web::scope("/actix")
                    .service(ex5_user_authentication::get_scoped_service(
                        "/users",
                        services.crypto_service().clone(),
                        repos.user_repository().clone()))
                    .service(ex3_postgres::get_scoped_service("/ping", repos.ping_repository().clone()))
                    .service(ex2_shared_state::get_scoped_service("/state"))
                    // Note that the ex1 service cannot be registered first
                    // because it would match before everything registered after it.
                    // The scope "" is used so that more than one services can
                    // can be registered on the root path without giving a
                    // mut ref to the ex1 mod.
                    .service(ex1_simple_routes::get_scoped_service(""))
            )
            // enables logging in the console in which the server runs through the actix middleware
            .wrap(actix_web::middleware::Logger::default())
    };
    
    tracing::info!("The server is running on {}:{}", env.host.as_str(), env.port);

    // the following should never stop looping
    actix_web::HttpServer::new(app_closure)
        .bind_openssl((env.host.as_str(), env.port), builder)?
        .run()
        .await?;
    Ok(())
}

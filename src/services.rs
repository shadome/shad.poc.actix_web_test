pub(crate) mod crypto;
pub(crate) use crypto::*;

#[derive(Clone, Debug)]
pub(crate) struct Services {
    crypto_service: CryptoService,
}

impl Services {
    
    #[tracing::instrument(skip(secret_key))]
    pub(crate) fn new(secret_key: secrecy::SecretString) -> Self {
        Self {
            crypto_service: CryptoService::new(secret_key),
        }
    }
    
    pub(crate) fn crypto_service(&self) -> &CryptoService { &self.crypto_service }
}
pub(crate) mod user_count;
pub(crate) mod user_create;
pub(crate) mod user_delete;
pub(crate) mod user_read;

#[derive(Clone, Debug)]
pub(crate) struct UserRepository {
    pg_pool: std::sync::Arc<sqlx::postgres::PgPool>,
}

impl UserRepository {
    
    pub(super) fn new(pg_pool: std::sync::Arc<sqlx::postgres::PgPool>) -> Self {
        Self { pg_pool: pg_pool }
    }

}

#[derive(Clone, Debug, Validate)]
pub(crate) struct User {
    pub(crate) id: uuid::Uuid,
    pub(crate) email: String,
    pub(crate) date_creation: chrono::DateTime<chrono::Utc>,
    pub(crate) password_hash: String,
}


impl super::User {

    #[tracing::instrument(skip(raw_password))]
    pub(crate) fn new_creatable(
        crypto_service: &crate::CryptoService,
        email: String,
        raw_password: String,
    ) -> Result<UserCreatable, crate::AppError> {
        let user = super::User {
            id: uuid::Uuid::new_v4(),
            email: email,
            date_creation: chrono::Utc::now(),
            password_hash: crypto_service.hash_password(raw_password.into())?,
        };
        validator::Validate::validate(&user)?;
        let result = UserCreatable { user };
        Ok(result)
    }

}

#[derive(Debug)]
pub(crate) struct UserCreatable { user: super::User }

impl UserCreatable {
    pub(crate) fn id(&self) -> uuid::Uuid { self.user.id }
    pub(crate) fn email(&self) -> &String { &self.user.email }
    pub(crate) fn date_creation(&self) -> chrono::DateTime<chrono::Utc> { self.user.date_creation }
    pub(crate) fn password_hash(&self) -> &String { &self.user.password_hash }
}

impl super::UserRepository {

    pub(crate) async fn create(&self, user: &UserCreatable) -> Result<(), crate::AppError> {
        sqlx
            ::query!(
                r#"insert into t_user(id, email, date_creation, password_hash) values ($1, $2, $3, $4);"#,
                user.id(),
                user.email(),
                user.date_creation(),
                user.password_hash())
            .execute(self.pg_pool.as_ref())
            .await?;
        Ok(())
    }

}

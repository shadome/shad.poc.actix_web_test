
pub(crate) struct UserCount { value: i64 }

impl UserCount {
    pub(crate) fn as_i64(&self) -> i64 { self.value }
}

impl super::UserRepository {

    pub(crate) async fn count(&self) -> Result<UserCount, crate::AppError> {
        let result = sqlx
            ::query_as!(
                UserCount,
                r#"select count(*) as "value!" from t_user;"#)
            .fetch_one(self.pg_pool.as_ref())
            .await?;
        Ok(result)
    }

}

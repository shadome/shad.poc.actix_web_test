
impl super::UserRepository {

    pub(crate) async fn read(
        &self,
        ids: Option<Vec<uuid::Uuid>>,
        emails: Option<Vec<String>>,
    ) -> Result<Vec<crate::User>, crate::AppError> {
        for email in emails.clone().unwrap_or_else(|| Vec::<String>::new()) {
            tracing::warn!(email);
        }
        let result = sqlx
            ::query_as!(
                crate::User,
                r#"
                    select id, email, date_creation, password_hash 
                    from t_user
                    where 1=1 -- used to align following `and` clauses
                        and ($1::uuid[] is null or id = any($1))
                        and ($2::varchar[] is null or email = any($2))
                ;"#,
                ids.as_deref(),
                emails.as_deref())
            .fetch_all(self.pg_pool.as_ref())
            .await?;
        Ok(result)
    }

}


#[derive(Clone, Debug)]
pub(crate) struct PingRepository {
    pg_pool: std::sync::Arc<sqlx::postgres::PgPool>,
}

impl PingRepository {
    pub(super) fn new(pg_pool: std::sync::Arc<sqlx::postgres::PgPool>) -> Self {
        Self { pg_pool: pg_pool }
    }
}

#[derive(sqlx::FromRow)]
pub(crate) struct NowRow { 
    pub(crate) timestamp: chrono::DateTime<chrono::offset::Utc>,
}

impl PingRepository {

    pub(crate) async fn now(&self) -> Result<NowRow, crate::AppError> {
        let result = sqlx::query_as::<_, super::NowRow>("select now() as timestamp;")
            .fetch_one(self.pg_pool.as_ref())
            .await?;
        Ok(result)
    }

}

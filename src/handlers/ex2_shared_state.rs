// --
/// Test of a mutable shared state.
/// All non-std uses are explicit for more clarity
/// (some packages provide structs and mods of the same name).
///
/// Demo hotwo:
/// * enter the route 'http://127.0.0.1:8080/state/get' and
///     get noticed that no message has been set yet
/// * enter the route 'http://127.0.0.1:8080/state/set/val' with
///     the desired replacement for 'val', like 'foo' or 'bar'
/// * enter the route 'http://127.0.0.1:8080/state/get' a second
///     time and get back the value previously set
// --

/// Exposed service
#[tracing::instrument(skip_all)]
pub fn get_scoped_service(scope: &str) -> actix_web::Scope  {
    let service = actix_web::web::scope(scope)
        .service(routes::set)
        .service(routes::get);
    service
}

mod routes {

    #[actix_web::get("/set/{value}")]
    #[tracing::instrument(skip_all)]
    async fn set(
        state: actix_web::web::Data<crate::handlers::AppState>,
        path: actix_web::web::Path<String>,
    ) -> impl actix_web::Responder {
        let new_val = path.into_inner();
        *state.string.lock().unwrap() = new_val;
        actix_web::HttpResponse::Ok()
            .body("The new value has been set :)")
    }

    #[actix_web::get("/get")]
    #[tracing::instrument(skip_all)]
    async fn get(
        state: actix_web::web::Data<crate::handlers::AppState>,
    ) -> String {
        let current_val = state.string.lock().unwrap().clone();
        match current_val.as_str() {
            "" => String::from("No value has been set yet"),
            _ => current_val,
        }
    }
}

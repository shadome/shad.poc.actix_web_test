// --
/// Test of a postgresql connection with a database function execution.
/// All non-std uses are explicit for more clarity
/// (some packages provide structs and mods of the same name).
///
/// Along with the exposed route, the ping mod serves as an example
/// to set up the simplest way to fetch data from a Postgres database
/// in Rust using actix-web and deadpool as a pool manager without
/// an ORM (the target architecture uses stored procedures/functions).
/// For this example, the postgres role 'ping' and database 'ping'
/// must exist beforehand.
/// What could be made even simpler, even though most of the
/// following would be bad practise:
/// * Not using a pool manager
/// * Not using the config crate and not storing login information
///     in a config file (by instanciating a deadpool_postgres::Config
///     object directly)
/// * Instanciating the database pool in the query handler rather than
///     passing it using actix_web::web::Data
/// * Fetching data easier to parse which do not require chrono::DateTime
/// 
/// Edit: ex3 now uses the Repository layer, which adds a bit of architectural
/// complexity.
// --

/// Exposed service
#[tracing::instrument(skip_all)]
pub(crate) fn get_scoped_service(
    scope: &str,
    repo: crate::PingRepository,
) -> actix_web::Scope  {
    // Note that get_scoped_service is called once upon initialisation,
    // whereas route functions are called for every client request.
    // Server-wide objects must be initialised outside of the route
    // functions and must be passed to them as Data-enclosed parameters.
    // In this case, it would be very wrong to instanciate the database
    // connection pool in the `get_ping` route: would work fine when
    // testing on localhost, but would not work with a production app
    // having multiple users.
    // Note to myself: still not sure if route initialisation, inside
    // the closure provided to `actix_web::HttpServer::new`, are called
    // on initialisation of a new client or of the whole server (I
    // suspect the former). Anyhow, anything which CAN be initialised
    // outside of the closure provided to actix_web::HttpServer::new
    // MUST be, and concurrent access should be handled with clones 
    // or std::sync::Arc.
    let service = actix_web::web::scope(scope)
        .service(routes::get_ping)
        .app_data(actix_web::web::Data::new(repo));
    service
}

mod routes {

    /// Returns the current timestamp from the database
    /// to prove that the connection to the server
    /// is up and working as intended.
    #[actix_web::get("")] // unfolded => /actix/ping
    #[tracing::instrument(skip_all)]
    async fn get_ping(
        repo: actix_web::web::Data<crate::PingRepository>,
    ) -> actix_web::HttpResponse {
        match repo.now().await
        {
            Ok(row) =>  actix_web::HttpResponse::Ok()
                .body(row.timestamp.format("%+").to_string()),
            Err(err) => actix_web::HttpResponse::InternalServerError()
                .body(format!("Error executing query: {}", err)),
        }
    }
    
}

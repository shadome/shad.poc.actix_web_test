// --
/// Initial tutorial from actix-web.
/// All non-std uses are explicit for more clarity
/// (some packages provide structs and mods of the same name).
// --

mod user_detail;
mod user_list;
mod user_sign_in;
mod user_sign_up;

/// Exposed service
#[tracing::instrument(skip_all)]
pub(crate) fn get_scoped_service(
    scope: &str,
    crypto: crate::CryptoService,
    repo: crate::UserRepository,
) -> actix_web::Scope  {
    let service = actix_web::web::scope(scope)
        .service(user_sign_up::get)
        .service(user_sign_up::submit)
        .service(user_sign_in::get)
        .service(user_sign_in::submit)
        .service(user_sign_in::private)
        .service(user_sign_in::log_out)
        .service(user_list::get)
        .service(user_list::do_delete_all)
        // this route must be set last because it will try to match and parse
        // as a uuid any route like "/actix/users/...", including "/actix/users/delete"
        .service(user_detail::get)
        .app_data(actix_web::web::Data::new(crypto))
        .app_data(actix_web::web::Data::new(repo));
    service
}

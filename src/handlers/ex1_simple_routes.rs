// --
/// Initial tutorial from actix-web.
/// All non-std uses are explicit for more clarity
/// (some packages provide structs and mods of the same name).
// --

/// Exposed service
#[tracing::instrument(skip_all)]
pub fn get_scoped_service(scope: &str) -> actix_web::Scope  {
    let service = actix_web::web::scope(scope)
        // routes with the `actix_web::get` attribute
        .service(routes::hello)
        .service(routes::echo)
        // route set manually
        .route("/hey", actix_web::web::get().to(manual_hello));
    service
}

/// Note that #[actix_web::get(/post/etc.)] seems to transform a method
/// into a struct, which makes it pub by default, thus exposed at an
/// overly wide scope.
/// Creating a private module rectifies this behaviour.
/// The content of this mod should be considered as private fn's,
/// as if the mod did not exist (e.g., manual_hello).
mod routes {

    // #[actix_web::get("{tail:.*}")]
    #[actix_web::get("")]
    #[tracing::instrument(skip_all)]
    async fn hello() -> impl actix_web::Responder {
        actix_web::HttpResponse::Ok()
            .body("Hello world!")
    }

    #[actix_web::post("/echo")]
    #[tracing::instrument(skip_all)]
    async fn echo(req_body: String) -> impl actix_web::Responder {
        actix_web::HttpResponse::Ok()
            .body(format!("echo: {}", req_body))
    }

}

#[tracing::instrument(skip_all)]
async fn manual_hello() -> impl actix_web::Responder {
    actix_web::HttpResponse::Ok()
        .body("Hey there!")
}

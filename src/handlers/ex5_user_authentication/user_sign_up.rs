
#[derive(askama::Template)]
#[template(path = "../src/handlers/ex5_user_authentication/user_sign_up.html")]
struct UserSignUpTemplate {
    // relevant if the page is reloaded with an error when a form submission failed
    validation_error: Option<String>,
    // relevant if the page is reloaded with an error when a form submission failed - use to persist the value of the email field before submission
    prev_input_email: String,
}

#[actix_web::get("/sign_up")]
pub(super) async fn get() -> Result<actix_web::HttpResponse, actix_web::error::Error> {
    let template_data = UserSignUpTemplate {
        validation_error: None,
        prev_input_email: String::default(),
    };
    crate::render_http_response(&template_data)
}

#[derive(Debug, serde::Deserialize, Validate)]
pub(super) struct UserSignUpForm {
    #[validate(email(message = "Please enter a valid email."))]
    email: String,
    #[validate(length(min = 8, max = 40, message = "The password must be at least 8 and at most 40 characters long."))]
    password: String,
}

#[actix_web::post("/sign_up")]
#[tracing::instrument(skip(form))]
pub(super) async fn submit(
    crypto: actix_web::web::Data<crate::CryptoService>,
    repo: actix_web::web::Data<crate::UserRepository>,
    form: actix_web::web::Form<UserSignUpForm>,
) -> Result<actix_web::HttpResponse, actix_web::error::Error> {
    // ensure the form is properly filled
    let form = form.into_inner();
    if let Err(e) = validator::Validate::validate(&form) {
        let template_data = UserSignUpTemplate {
            validation_error: Some(crate::AppError::from(e).message.to_owned()),
            prev_input_email: form.email.to_owned(),
        };
        return crate::render_http_response(&template_data);
    }
    // ensure the provided email address does not already exist
    if repo.read(None, Some(vec![form.email.to_owned()])).await?.len() > 0 {
        let template_data = UserSignUpTemplate {
            validation_error: Some("A user with this email address already exists.".into()),
            prev_input_email: form.email.to_owned(),
        };
        return crate::render_http_response(&template_data);
    }
    // ensure there are not more than 5 existing accounts (upper limit)
    let upper_bound = 5;
    if repo.count().await?.as_i64() >= upper_bound {
        let template_data = UserSignUpTemplate {
            validation_error: Some("Maximum number of users reached, delete some users if you want to create more.".into()),
            prev_input_email: form.email.to_owned(),
        };
        return crate::render_http_response(&template_data);
    }
    // create a new user account
    let new_user = crate::User::new_creatable(
        crypto.as_ref(),
        form.email.into(),
        form.password.into(),
    )?;
    let _ = repo.create(&new_user).await?;
    // go back to users page
    crate::redirect("/actix/users")
}


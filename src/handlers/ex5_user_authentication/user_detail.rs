
#[derive(askama::Template)]
#[template(path = "../src/handlers/ex5_user_authentication/user_detail.html")]
struct UserDetailTemplate {
    user: crate::User,
}

#[actix_web::get("/{id}")]
#[tracing::instrument(skip_all)]
pub(super) async fn get(
    repo: actix_web::web::Data<crate::UserRepository>,
    id: actix_web::web::Path<uuid::Uuid>,
) -> Result<actix_web::HttpResponse, actix_web::error::Error> {

    let mut users = repo
        .read(Some(vec![id.into_inner()]), None)
        .await?;
    if users.len() != 1 {
        Err(crate::AppError { message: format!("User not found ({} users).", users.len()) })?;
    }
    let template_data = UserDetailTemplate { 
        user: users.remove(0),
    };
    crate::render_http_response(&template_data)
}

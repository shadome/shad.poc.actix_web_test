
#[derive(askama::Template)]
#[template(path = "../src/handlers/ex5_user_authentication/user_sign_in.html")]
struct UserSignInTemplate {
    // relevant if the page is reloaded with an error when a form submission failed
    validation_error: Option<String>,
    // relevant if the page is reloaded with an error when a form submission failed - use to persist the value of the email field before submission
    prev_input_email: String,
}

#[actix_web::get("/sign_in")]
pub(super) async fn get(
    identity: Option<actix_identity::Identity>,
) -> Result<actix_web::HttpResponse, actix_web::error::Error> {
    // ensure the client is not logged in already
    if identity.is_some() {
        return crate::redirect("/actix/users");
    }
    let template_data = UserSignInTemplate {
        validation_error: None,
        prev_input_email: String::default(),
    };
    crate::render_http_response(&template_data)
}

#[derive(Debug, serde::Deserialize, Validate)]
pub(super) struct UserSignInForm {
    #[validate(email(message = "Please enter a valid email."))]
    email: String,
    #[validate(length(min = 8, max = 40, message = "Please enter a valid password."))]
    password: String,
}

#[actix_web::post("/sign_in")]
#[tracing::instrument(skip(form, identity))]
pub(super) async fn submit(
    identity: Option<actix_identity::Identity>,
    req: actix_web::HttpRequest,
    crypto: actix_web::web::Data<crate::CryptoService>,
    repo: actix_web::web::Data<crate::UserRepository>,
    form: actix_web::web::Form<UserSignInForm>,
) -> Result<actix_web::HttpResponse, actix_web::error::Error> {
    // ensure the client is not logged in already
    if identity.is_some() {
        return crate::redirect("/actix/users");
    }
    // ensure the form is properly filled
    let form = form.into_inner();
    if let Err(e) = validator::Validate::validate(&form) {
        let template_data = UserSignInTemplate {
            validation_error: Some(crate::AppError::from(e).message.to_owned()),
            prev_input_email: form.email.to_owned(),
        };
        return crate::render_http_response(&template_data);
    }
    // get the row matching the provided email address
    let user = match repo.read(None, Some(vec![form.email.to_owned()])).await? {
        rows if rows.len() == 0 => {
            let template_data = UserSignInTemplate {
                validation_error: Some("Incorrect email and/or password.".into()),
                prev_input_email: form.email.to_owned(),
            };
            return crate::render_http_response(&template_data);
        },
        mut rows => rows.remove(0),
    };
    // ensure the password matches the stores hash
    if !crypto.compare_passwords(form.password.into(), user.password_hash)? {
        let template_data = UserSignInTemplate {
            validation_error: Some("Incorrect email and/or password.".into()),
            prev_input_email: form.email.to_owned(),
        };
        return crate::render_http_response(&template_data);
    }
    // create the authentication session cookie to persist the logged-in state using actix_identity
    actix_identity::Identity
        ::login(
            &actix_web::HttpMessage::extensions(&req),
            user.id.to_string())
        .unwrap(); // Note: I am not adding the `anyhow` dependency just to enable the ? syntax for this function.
    // go back to users page
    crate::redirect("/actix/users")
}

#[actix_web::get("/private")]
#[tracing::instrument(skip(identity))]
pub(super) async fn private(
    identity: Option<actix_identity::Identity>,
) -> Result<actix_web::HttpResponse, actix_web::error::Error> {
    return if let Some(not_logged_in_return_value) = crate::ensure_is_logged_in(&identity) {
        not_logged_in_return_value
    } else {
        Ok(actix_web::HttpResponse::Ok().body("you are logged in :)"))
    }
}

#[actix_web::post("/log_out")]
#[tracing::instrument(skip(identity))]
pub(super) async fn log_out(
    identity: Option<actix_identity::Identity>,
) -> Result<actix_web::HttpResponse, actix_web::error::Error> {
    if let Some(not_logged_in_return_value) = crate::ensure_is_logged_in(&identity) {
        return not_logged_in_return_value;
    }
    let identity = identity.unwrap();
    identity.logout();
    crate::redirect("/actix/users")
}

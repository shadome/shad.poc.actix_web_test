
#[derive(askama::Template)]
#[template(path = "../src/handlers/ex5_user_authentication/user_list.html")]
struct UserListTemplate {
    is_client_logged_in: bool,
    users: Vec<crate::User>,
}

#[actix_web::get("")]
#[tracing::instrument(skip_all)]
pub(super) async fn get(
    identity: Option<actix_identity::Identity>,
    repo: actix_web::web::Data<crate::UserRepository>,
) -> Result<actix_web::HttpResponse, actix_web::error::Error> {
    let users = repo.read(None, None).await?;
    let is_client_logged_in = identity.is_some();
    let template_data = UserListTemplate { users, is_client_logged_in };
    crate::render_http_response(&template_data)
}

/// Since it is not possible to send anything other than GET and POST HTTP requests
/// methods from pure HTML, a POST is used for deletion.
#[actix_web::post("/delete")]
pub(super) async fn do_delete_all(
    repo: actix_web::web::Data<crate::UserRepository>,
) -> Result<actix_web::HttpResponse, actix_web::error::Error> {
    let _ = repo.delete(None).await?;
    crate::redirect("/actix/users")
}

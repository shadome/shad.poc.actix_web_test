use std::error::Error;
use std::fmt;

/* AppError implementation, for simpler error management */

#[derive(Debug)]
pub(crate) struct AppError {
    pub(crate) message: String,
}

impl fmt::Display for AppError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl Error for AppError {}

/* From AppError to actix ResponseError, for enabling error propagation syntax ("await?", etc.) in handlers */

impl actix_web::error::ResponseError for AppError {
    fn error_response(&self) -> actix_web::HttpResponse {
        actix_web::HttpResponse::InternalServerError().body(self.message.to_owned())
    }
}

/* From everything to AppError, for simpler error management */


impl From<std::io::Error> for AppError {
    fn from(error: std::io::Error) -> Self {
        AppError {
            message: error.to_string(),
        }
    }
}

impl From<argonautica::Error> for AppError {
    fn from(error: argonautica::Error) -> Self {
        AppError {
            message: error.to_string(),
        }
    }
}

impl From<sqlx::Error> for AppError {
    fn from(error: sqlx::Error) -> Self {
        AppError {
            message: format!("{:?}", error),
        }
    }
}

impl From<askama::Error> for AppError {
    fn from(error: askama::Error) -> Self {
        AppError {
            message: format!("{:?}", error),
        }
    }
}

impl From<validator::ValidationErrors> for AppError {
    /// careful about semantics: validator uses the `error` dataflow of Result to pass down several errors
    /// so the parameter named error represent in fact errors  
    fn from(error: validator::ValidationErrors) -> Self {
        let message = if error.is_empty() {
            "empty ValidationErrors though we're in the error dataflow, should never happen".into()
        } else {
            // Note: the modelling of the validator crate's errors does not provide a deterministic sorting: the same errors can appear in different orders.
            // The following shenanigans ensure error sorting before serialisation.
            let mut vector = error
                .into_errors()
                .into_iter()
                // only keeping the error keys unless if it a `Field` validation error kind (only one used in the app as of now)
                .map(|err| match err.1 {
                    validator::ValidationErrorsKind::Struct(_) => err.0.into(),
                    validator::ValidationErrorsKind::List(_) => err.0.into(),
                    validator::ValidationErrorsKind::Field(field) => field.iter().filter_map(|f| f.message.as_deref()).collect::<Vec<&str>>().join(", "),
                })
                .collect::<Vec<_>>();
            vector.sort();
            vector.join("\n")
        };
        AppError { message }
    }
}

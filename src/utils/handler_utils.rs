pub(crate) fn render_http_response<T: askama::Template>(
    template_data: &T,
) -> Result<actix_web::HttpResponse, actix_web::error::Error> {
    let body = match askama::Template::render(template_data) {
        Ok(body) => body,
        Err(err) => Err(crate::AppError { message: format!("{:?}", err) })?,
    };
    let http_response = actix_web::HttpResponse::Ok()
        .content_type("text/html")
        .body(body);
    Ok(http_response)
}

pub(crate) fn redirect(
    path: &str
) -> Result<actix_web::HttpResponse, actix_web::error::Error> {
    let http_response = actix_web::HttpResponse::Found()
        .append_header(("Location", path))
        .finish();
    Ok(http_response)
}

pub(crate) fn ensure_is_logged_in(
    identity: &Option<actix_identity::Identity>,
) -> Option<Result<actix_web::HttpResponse, actix_web::error::Error>> {
    match identity.as_ref().map(|x| x.id()) {
        None => Some(redirect("/actix/users/sign_in")),
        Some(Ok(_)) => None,
        Some(Err(err)) => Some(Err(actix_web::error::Error::from(crate::AppError { message: format!("{:?}", err) }))),
    }
}
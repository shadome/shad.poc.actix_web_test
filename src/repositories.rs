pub(crate) mod user;
pub(crate) use user::*;
pub(crate) mod ping;
pub(crate) use ping::*;

#[derive(Clone, Debug)]
pub(crate) struct Repositories {
    user_repository: UserRepository,
    ping_repository: PingRepository,
}

impl Repositories {
    
    pub(crate) fn new(pg_pool: sqlx::postgres::PgPool) -> Self {
        let pg_pool = std::sync::Arc::new(pg_pool);
        Self {
            user_repository: UserRepository::new(pg_pool.clone()),
            ping_repository: PingRepository::new(pg_pool.clone()),
        }
    }
    
    pub(crate) fn user_repository(&self) -> &UserRepository { &self.user_repository }
    pub(crate) fn ping_repository(&self) -> &PingRepository { &self.ping_repository }
}
// See https://www.youtube.com/watch?v=w6mobzGwN-Q&list=PLECOtlti4Psqw1qRaN4R9sWSQWvqfJU_V&index=5


#[derive(Clone, Debug)]
pub(crate) struct CryptoService {
    secret_key: secrecy::SecretString,
}

impl CryptoService {

    pub(super) fn new(secret_key: secrecy::SecretString) -> Self {
        Self { secret_key }
    }

    pub(crate) fn hash_password(&self, raw_password: secrecy::SecretString) -> Result<String, crate::AppError> {
        // traces because it is slow
        tracing::info!("hashing a password...");
        let hash = argonautica::Hasher::default()
            .with_secret_key(secrecy::ExposeSecret::expose_secret(&self.secret_key))
            .with_password(secrecy::ExposeSecret::expose_secret(&raw_password))
            .hash()?;
        tracing::info!("done hashing a password");
        Ok(hash)
    }

    pub(crate) fn compare_passwords(
        &self,
        raw_password: secrecy::SecretString,
        password_hash: String,
    ) -> Result<bool, crate::AppError> {
        // traces because it is slow
        tracing::info!("comparing passwords...");
        let verified = argonautica::Verifier::default()
            .with_secret_key(secrecy::ExposeSecret::expose_secret(&self.secret_key))
            .with_hash(password_hash)
            .with_password(secrecy::ExposeSecret::expose_secret(&raw_password))
            .verify()?;
        tracing::info!("done comparing passwords");
        Ok(verified)
    }

}

#[tracing::instrument]
pub(super) fn init_logging_and_generate_some_logs() {
    tracing_subscriber::fmt()
        .event_format(
            tracing_subscriber::fmt::format()
                //.with_level(false) // don't include levels in formatted output
                //.with_target(false) // don't include targets
                //.with_thread_ids(true) // include the thread ID of the current thread
                //.with_thread_names(true) // include the name of the current thread
                .compact())
        .init();
    tracing::info!("ex4 logging info :)");
    tracing::warn!("ex4 logging warning :o");
    tracing::error!("ex4 logging error :(");
}
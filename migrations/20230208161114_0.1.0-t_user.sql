-- Add migration script here

create table if not exists t_user (
    id uuid primary key,
    email varchar not null,
    password_hash varchar not null,
    date_creation timestamp with time zone not null);
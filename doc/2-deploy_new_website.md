
# Setup `nginx`

## Configure a new reverse proxy entry

Edit the file `/etc/nginx/nginx.conf` and add a new **location** entry.
```sh
server {
  # ...
  
  # replace / with the required path stub
  location / { 
    # replace 8000 with the port the server will use when deployed on localhost
    # it should not conflict with ports already used
    proxy_pass http://0.0.0.0:8000; 
    proxy_buffering off;
    proxy_set_header X-Real-IP $remote_addr;
  }
}
```
Then, restart **nginx**.
```sh
systemctl restart nginx
```

## Launch the server once to try it out

This step does not consider any CICD yet.

Get your server in the form of an executable, using **scp** or compiling it on the VPS. Then, execute the server, which typically outputs something like the following.
```sh
$ ./<new_server_binary>
The server is now listening on http://0.0.0.0:8000.
```

The server's routes should now be accessible online.

# Setup the system

## Setup a system service
The new site should run as a system daemon so that it will automatically start when the system boots up, and will run in the background, without needing an open terminal.

Create a file under `/etc/systemd/system/<new_site_service>.service`.
Example of content: 
```sh
[Unit]
Description=<description>

[Service]
User=<user dedicated to this new server>
# e.g. /home/new-site/
WorkingDirectory=/home/<new_site_user>/<new_site_folder>/
# e.g. /home/new-site/target/release/new-site.binary
ExecStart=/home/<new_site_user>/<new_site_folder>/<path to executable>

[Install]
WantedBy=multi-user.target
```
> Note: for most applications, the binary execution will fail if not started from the working directory *(e.g., the root folder under `/src/`, `/target/release/`, etc.)*.

Now, enable the service to start when the system boots up and also start it right now.

```sh
systemctl enable <new_site_service>.service
systemctl start <new_site_service>.service
```

## Create a dedicated technical user

For more security and better folder architecture, a new user will be added for each new web server. 
> For simplicity, the name of the new website project can be used for every user, group, database, etc. which will be created.

> Note that this command creates a `/home/<user_name>` folder for the new user.

```sh
useradd -m <user_name>
```

This new user must be allowed to restart the running server service without being prompted for a password. Run `visudo`, then add the following row:
```sh
<user_name> ALL= NOPASSWD: /bin/systemctl restart <new_site_service>.service
```

# Setup `postgresql` for an application

Log in to the PostgreSQL prompt as the **postgres** user:
```sh
sudo -u postgres psql
```
Create a new database and role matching the new website.
> For simplicity, the role, the user and the database all have the same name. 
```sql
create role <role_name>;
alter role <role_name> with login encrypted password 'scram-sha-256';
alter role <role_name> with login password '<password>';
create database <database_name> owner <role_name>;
```
Then, revoke any privilege on this new database from existing roles and grant every privilege on this new database to this new role.
```sql
grant all privileges on database <database_name> to <role_name>;
revoke connect on database <database_name> from public;
```

Finally, configure the website source code, for instance by updating or creating a new `.env` file in the project directory and adding the following environment variable:
```toml
DATABASE_URL=postgres://<username>:<password>@localhost/<database_name>
```

# Setup continuous deployment

This step requires the application to be hosted on gitlab, e.g., [this poc](https://gitlab.com/shadome/shad.poc.actix_web_test/). It is strongly inspired from [this tutorial](https://www.agileleaf.com/blog/set-up-gitlab-for-continous-deployments/).


> Continuous deployment could be summarised in a few words as the art of dodging password prompts while only allowing the strictly necessary number of hosts to connect on a restricted portion of a VPS and run the strictly necessary set of commands to update, recompile and restart an always-running application.
>
> Avoiding password prompts is considered a good practice because it eliminates the need to keep track of where plain-text passwords are stored in configuration files or host variables.
>
> Also, authentication techniques such as RSA SSH keys, which will be used in the following steps, are considered more secure than password prompts. Using public-key cryptography means that only the public keys are transferred, and even if the key is intercepted, it cannot be used without the corresponding private key. Passwords can be guessed or intercepted on every login request.

## Create an RSA public/private key pair

```sh
cd ~/.ssh
ssh-keygen -t rsa
```
Then, add the created public key to the known hosts:
```sh
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys 
```
## Register and enable the public key in the GitLab repository

Go to the directory where the keys were generated and copy the content of the `id_rsa.pub` file.

In GitLab, navigate to `<your project> / Settings / Repository / Deploy keys` and add a new deploy key.

## Configure the server with the new key

Ensure `ssh-agent` is enabled.
```sh
eval $(ssh-agent -s)
```
Add the private key to the SSH registry.
```sh
ssh-add ~/.ssh/id_rsa
```
Create or edit the file `~/.ssh/config` and insert the following content.
```sh
Host gitlab.com
PubkeyAuthentication yes
IdentityFile ~/.ssh/id_rsa
``` 
Check that GitLab answers your `ssh` queries without prompting for a password. A certificate may need to be added. The following command should answer with your username: `Welcome to GitLab, @username`.
```sh
ssh -T git@gitlab.com
```
## Clone the GitLab repository

Go to your repository's main page, click the **Clone** button and select **Clone with SSH**.

On the server, create a dedicated folder for this application and clone it there.
```sh
cd ~/sites
git clone git@gitlab.com:shadome/shad.poc.actix_web_test.git
git ls-remote # check the repository access
```
## Configure the connexion between GitLab and the server
The simplest way to setup continuous deployment is to configure GitLab to access the server and execute some commands through a Docker runner.
### Note
At this point it is important to understand that you will share your private SSH login key, which means you fully trust GitLab to neither misuse it nor let it leak. More secure ways of achieving CD may require additional configuration: create an SSH user on the server with very limited rights, split your server into closed VMs, install specific runners directly on the server, etc. *(not documented here)*.
### Configuration
Copy the private SSH key.
```sh
cat ~/.ssh/id_rsa # then copy the output
```
Go to the GitLab repository: `Settings / CI/CD / Variables / Add a variable` and insert the copied value as a new variable named, for instance, `SSH_PRIVATE_KEY`.
## Create the GitLab pipeline for CD
At this point, the server and GitLab can both communicate with each other without requiring password prompts. Continuous deployment roughly consists in the following steps, at least in this setup tutorial:
* GitLab listens to any `git push` made by any client to the application repository
* if a push occurs, GitLab starts a worker configured though the `.gitlab-ci.yml` file
* the worker connects to the server, and then:
* reaches the right directory and pulls the up-to-date version of the source code
   * at this point, GitLab connects to the server which in turn connects to GitLab with `git pull`
* compiles the pulled code into a binary
* stops the current server process exposed via `nginx`
* starts a process for the new server binary
### Isolate commands to run manually

This step is meant to define manually the command-line instructions to achieve what has been described above.

Open a new terminal and connect to the VPS. Switch user if required to login as the user which will run the website. Define the commands required to update and restart the running website, e.g.:
```sh
cd <website_folder>/
git checkout main # or any other name for the production branch
git pull origin main # or any other name for the production branch
cargo build --release # or any other building tool for the web application
sudo systemctl restart <new_site_service>.service
```
Then, make sure the updated version of your code is currently served.

### Configure the `gitlab-ci.yml` file
This step consists in creating or editing a `.gitlab-ci.yml` file. It can be done by adding this file to the root of the application or through the GitLab editor interface `CI/CD / Editor / Edit - Commit`.

Copy the typical content below. Edit the `script` part to match what must be done manually to update the source code of the application, create a server binary file and put the binary in the right place for `nginx` to serve.

```yml
image: alpine

stages:
  - deployment

deploy:
  stage: deployment
  before_script:
    - 'which ssh-agent || ( apk add --update openssh-client )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY")
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - ssh <website_user>@<vps_ip_address> "
        cd <website_folder>/ &&
        git checkout main &&
        git pull origin main &&
        cargo build --release &&
        sudo systemctl restart <new_site_service>.service &&
        exit"
  only:
    - main
```
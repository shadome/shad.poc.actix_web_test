# Motivation
The purpose of this guide is to configure a virtual private server, or VPS, as a server for deploying multiple web applications. A web server is a piece of software listening to a certain port of a machine (the server) which is itself accessed through an IP address.

A server deployed on the local machine, during development, typically notifies the developer of something like:
```sh
The server is running on 127.0.0.1:8080
```

If the development stage uses flexible ports, it should not be the case for web applications accessed through the Internet with a web browser. There is a set of reserved port numbers from which a server of web applications is expected to provide a website using the `http` (port 80) or `https` (port 443) protocols.

Standalone web servers (Node.Js, Rocket, ASP.NET, etc.) cannot coexist on the same combination of addresses and port numbers. Serving them concurrently requires setting up a *reverse proxy*, which can be defined as a piece of software listening to the requests coming from the Internet and dispatching them according to some user-defined rules.

This guide uses `nginx` to dispatch requests according to a straightforward URL pattern. With this configuration, the servers developed and hosted concurrently only have to define a unique port number and route prefix. For instance, let us imagine a request for any URL matching the following patterns, sent from any browser on any client connected to the internet, where `89.214.85.211` is the VPS's IPv4 publicly accessible address. Respectively:
* `http://89.214.85.211/webapp1/<rest of the URI>` could be dispatched to a first web server running locally on `127.0.0.1:54321` and handling routes starting with `webapp1`,
* `http://89.214.85.211/webapp2/<rest of the URI>` could be dispatched to a second web server running locally on `127.0.0.1:12345` and handling routes starting with `webapp2`,
* etc.

# Install useful and required packages
Connect to the server:
```sh
ssh <username>@<ip>
```
Install packages useful for administering the server:
```sh
su
pacman -Syu
pacman -S nano
# rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
rustup update
pacman -S cargo
# dep for argonautica, which uses cc and bindgen to compile the canonical C implemenation of Argon2 into a static archive during the build process.
# see https://github.com/bcmyers/argonautica/tree/master/argonautica-rs#installation
pacman -S clang  
# sqlx-cli database migration tool for postgresql databases, see https://crates.io/crates/sqlx-cli
pacman -S openssl pkg-config
cargo install sqlx-cli --no-default-features --features native-tls,postgres
#etc.
```

# Setup `nginx`

## Installation
```sh
pacman -S nginx
```
The `nginx` daemon should be running, else start and enable it.
```sh
systemctl enable nginx # start, restart, stop, etc.
```
## File and folder architecture overview
`nginx` is configured in the `/etc/nginx/nginx.conf` file. Most of the default configuration is unnecessary for a basic usage.

A `location` object must be added for each site to reverse proxy towards, under the `server` object, itself nested under `http`. If a custom location path is specified (anything after `/`), make sure to forward it in the `proxy_pass` as well, in which case the site must acknowledge this prefix too for each of its exposed routes.

Example of `nginx.conf` content for two websites currently running on localhost on ports 54321 and 54322. In this example, if the server `webapp1` is configured with a home page, the associated route should be `/webapp1/home`.
```sh
worker_processes  1;
events {
    worker_connections  1024;
}
http {
    include mime.types;
    server {
        listen 80;
        listen [::]:80;
        server_name shad.net;
        # Mind the order of the location's paths, first match is returned
        location /webapp1 {
            proxy_pass http://0.0.0.0:54321/webapp1;
        }
        location /webapp2 {
            proxy_pass http://0.0.0.0:54322/webapp2;
        }
    }
    default_type application/octet-stream;
    sendfile on;
    keepalive_timeout 65;
}
```
Restarting the service is required whenever changes are made to configuration files.

## Note
Most static websites require static content to be served along with usual HTTP(S) routes, such as css styling or javascript files. Those files should be served by the web server itself, to avoid additional location setup is required with `nginx` and to avoid filename collision.

# Setup `PostgreSql`
[Following the archlinux wiki](https://wiki.archlinux.org/title/PostgreSQL)
## Installation
By default, everything about the `postgresql` installation can be found under `/var/lib/postgres`.
### Install the package
```sh
pacman -S postgresql
```
### Initial configuration of the database cluster
```sh
# Connect as the postgres user
su -l postgres
# Initialise the database
[postgres]$ initdb --locale=C.UTF-8 --encoding=UTF8 -D /var/lib/postgres/data --data-checksums
# At this point, the database "postgres" has been created and is accessible to the user also named "postgres"
[postgres]$ psql -d postgres
# List existing users and their roles
postgres=# \du 
# List existing databases
postgres=# \l 
```
### Start and enable the postgresql service
```sh
systemctl start postgresql.service
systemctl enable postgresql.service
```
## Configuration
By default, any local user can connect as any database user, including the database superuser. To change this behaviour, edit `/var/lib/postgres/data/pg_hba.conf` and change the line (1) to (2).
```sh
# TYPE  DATABASE        USER            ADDRESS                 METHOD
#local   all             all                                     trust #(1)
local   all             postgres                                peer   #(2)
```
### Disable unrequired connection methods by default
After that, remove any row which does not start with **local**, which should leave only one row. Then, add two additional rows, so that the configuration looks like the one below.
* The **host** type means the connection is allowed using TCP/IP, either SSL or non-SSL. It is configured with an address which only allows localhost connections for running applications.
* The **local** type means the connection is allowed using Unix sockets, i.e., terminals.
* The **sameuser** database means the rule matches databases with the same name of the provided user name.
* The **scram-sha-256** authentication method ensures connecting to a database requires a password, and password storage is encrpyted with an up-to-date encrpytion algorithm. This is not related to TLS/SSL options, which can be set to encrypt queries between an application and the database.
```sh
# TYPE  DATABASE        USER            ADDRESS                 METHOD
local   all             postgres                                scram-sha-256
local   sameuser        all                                     scram-sha-256
host    sameuser        all             127.0.0.1/32            scram-sha-256
```
For more information, check [this official documentation](https://www.postgresql.org/docs/current/auth-pg-hba-conf.html).

Then, edit `/var/lib/postgres/data/postgresql.conf` and set: 
```sh
password_encryption = scram-sha-256
```
Finally, restart `postgresql.service`, and then re-add each user's password using:
```sh
ALTER USER <user> WITH ENCRYPTED PASSWORD '<password>';
```

# Troubleshooting
Some of the problems which were encountered when following this guide.
## Xterm error
On some clients, using a text editor such as `nano` will raise an xterm error related to the available colors. This error must be fixed on the terminal of the client, not the remote server.
### Quickfix
To fix it for the current session, use:
```sh
export TERM=xterm-256color
```
### Permanent fix
To fix it permanently, you can either:
* align the `TERM` environment variable values of the client and the remote, or
* make a dedicated command which exports a value for the `TERM` environment variable compatible with the remote terminal before performing the `ssh` command
## `nginx` refuses to restart
If `nginx` refuses to restart, it may be required to kill it before starting it again.
```sh
pkill -f nginx & wait $!
systemctl start nginx
```
# Introduction

HTTPS is the secure version of the HTTP protocol used for transmitting data over the Internet. Under the hood, HTTPS uses Transport Layer Security (TLS) or its predecessor, Secure Sockets Layer (SSL), to encrypt the data being transmitted between a client (such as a web browser) and a server.

When setting up HTTPS, both a certificate and a private key are required. The certificate is a digital file that contains information about the website or organisation that it identifies, as well as the public key of the server. The private key, on the other hand, is a secret key that is used to decrypt the data that has been encrypted with the public key.

The typical steps are as follows:

1. When a client (such as a web browser) wants to establish an HTTPS connection with a server, it sends a message to the server, along with a list of supported cryptographic protocols and ciphers.
2. The server responds with a message that specifies the protocol and cipher that will be used for the connection.
3. The server sends its certificate to the client, along with a digitally-signed message that proves the authenticity of the certificate. The certificate contains the public key of the server, which will be used to encrypt the data being transmitted.
4. The client verifies the authenticity of the certificate by checking the digital signature, and then generates a symmetric key that will be used for encryption and decryption.
5. The client encrypts the symmetric key with the server's public key and sends it to the server.
6. The server decrypts the symmetric key with its private key, and then uses the symmetric key to encrypt the data being transmitted. The client also uses the symmetric key to decrypt the data being received.

In this process, the private key is kept secret and is only used by the server to decrypt data that has been encrypted with the public key. The certificate, which contains the public key, is used by the client to encrypt data that will be sent to the server. Together, the certificate and private key allow for secure communication between the client and server over an encrypted channel.

# Considerations

It is usually good practise to:
* set a validity period for a certificate
* not share a same certificate, e.g., between development and production environments
* ignore PEM files in the version control, e.g., add a rule in `.gitignore`
* obtain a trusted certificate from a trustworthy CA at least for production environments

# Obtain the certificate and the private key

Run a command similar to the one below to generate a new self-signed X.509 certificate and RSA private key.
```sh
openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 365 -out cert.pem
```

Breakdown of each option in the example:
* `-newkey rsa:2048` generates a new RSA private key with a key size of 2048 bits
* `-nodes` tells OpenSSL not to encrypt the private key
* `-keyout key.pem` specifies the output file name for the private key in PEM format
* `-x509` creates a self-signed X.509 certificate
* `-days 365` sets the validity period of the certificate to 365 days
* `-out cert.pem` specifies the output file name for the X.509 certificate in PEM format

# Setup HTTPS for `Actix-Web`

Move the previously generated PEM files to the project root directory. Ignore them in `.gitignore` if necessary.

Modify the `cargo.toml` to add the feature `openssl` to `Actix` and to add the dependency `openssl`:
```toml
actix-web = { version = "^4", features = ["openssl"] }
openssl = "0.10"
```

Configure the HTTP server to use the `key.pem` and `cert.pem` files for SSL encryption:

```rust
use actix_web::{web, App, HttpResponse, HttpServer};
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    builder.set_private_key_file("key.pem", SslFiletype::PEM).unwrap();
    builder.set_certificate_chain_file("cert.pem").unwrap();
    // ...
    HttpServer::new(|| {
        // ...
    })
    .bind_openssl("127.0.0.1:8443", builder)?
    .run()
    .await
}
```

# Update `nginx` for HTTPS

`nginx` needs a bit of configuration to reverse proxy HTTPS requests towards the updated website. There are many different ways to achieve this, but a simple configuration example of the file `/etc/nginx/nginx.conf` would be:

```conf
    server {
        listen 443 ssl;
        server_name <server_name>;

        ssl_certificate /path/to/certificate.pem;
        ssl_certificate_key /path/to/certificate/key.pem;

        location /<website_url_prefix> {
            proxy_pass https://0.0.0.0:<localhost_port>/<website_url_prefix>;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }
    }
```
Where:
* a new `server` block is added under the `http` block and specifies paths to the SSL certificate and encryption key
* the \<placeholders\> must be replaced with the actual values
* the `proxy_pass` clause now specifies an `https` URL (previously `http`)

Note that the previous `server` block used to handle HTTP requests can still serve the website over unsecured HTTP requests if necessary, but the `proxy_pass` clause still needs to specify an updated `https` URL. For example:

```conf
http {
    // ...
    
    server {
        listen 80;
        server_name <server_name>;

        location /<website_url_prefix> {
            proxy_pass https://0.0.0.0:<localhost_port>/<website_url_prefix>;
            // ...
        }

        // other http websites...
    }

    server {
        listen 443 ssl;
        server_name <server_name>;

        ssl_certificate /path/to/certificate.pem;
        ssl_certificate_key /path/to/certificate/key.pem;

        location /<website_url_prefix> {
            proxy_pass https://0.0.0.0:<localhost_port>/<website_url_prefix>;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

        // other https websites...
    }
}
```

Restart `nginx` after the configuration file changes:
```sh 
systemctl restart nginx
```
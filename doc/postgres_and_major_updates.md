# Issue

With Arch Linux, `postgres` updates come along when updating the system. If it is a major `postgres` update, e.g., `14.xx` to `15.xx`, it will break the existing `postgres` server and make databases unaccessible.

# Avoiding the issue

[See the official Arch wiki](https://wiki.archlinux.org/title/Pacman#Skip_package_from_being_upgraded).

# Migrating

[From the official Arch wiki](https://wiki.archlinux.org/title/PostgreSQL#Upgrading_PostgreSQL).


Install the new update and stop the `postgresql` service. 
> Important note: after this installation, even without properly stopping the service, the databases can no longer be reached, so the server backends will be broken for maintenance. An adequate maintenance message could be displayed (e.g., set and use an environment variable, undocumented yet).
```sh
pacman -S postgresql
systemctl stop postgresql
```
Install the migration tool, if already present for your old version in the official Arch repos.
```sh
pacman -S postgresql-old-upgrade
```
Open two terminals and run the following commands, either as a sudo user or as the `postgres` user, depending on the command:
```sh
[user]$ mv /var/lib/postgres/data /var/lib/postgres/olddata
[user]$ mkdir /var/lib/postgres/data
[user]$ chown postgres:postgres /var/lib/postgres/data
[postgres]$ cd
[postgres]$ initdb -D /var/lib/postgres/data --locale=C.UTF-8 --encoding=UTF8 --data-checksums
[postgres]$ /opt/pgsql-14/bin/pg_ctl -D /var/lib/postgres/olddata/ start
[user]$ cp /usr/lib/postgresql/postgis-3.so /opt/pgsql-14/lib/ # Only if postgis installed
[postgres]$ pg_dumpall -h /tmp -f /tmp/old_backup.sql
[postgres]$ /opt/pgsql-14/bin/pg_ctl -D /var/lib/postgres/olddata/ stop
```
